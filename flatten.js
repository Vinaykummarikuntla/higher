
exportObject = {
    flatten : (elements, flat = []) => {
        if(elements instanceof Array){
            for(element of elements) {
                if(element instanceof Array){
                    exportObject.flatten(element, flat);
                }
                else {
                    flat.push(element);
                }
            }
            return flat;
        }
        else {
            console.log('instanceError: Parameter passed is not an Array');
        }
    }
}
module.exports = exportObject;