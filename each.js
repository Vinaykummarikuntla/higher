module.exports = {
    each : (elements, cb, startingIndex = 0) => {
        if(elements instanceof Array){
            for(let index = startingIndex; index < elements.length; index++) {
                cb(elements[index], index);
            }
        }
        else {
            console.log('instanceError: Parameter passed is not an Array');
        }
    }
}
