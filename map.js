module.exports = {
    map : (elements, cb, startingIndex = 0) => {
        if(elements instanceof Array){
            const mapped = [];
            for(let index = startingIndex; index < elements.length; index++) {
                mapped.push(cb(elements[index]));
            }
            return mapped;
        }
        else {
            console.log('instanceError: Parameter passed is not an Array');
        }
    }
}
