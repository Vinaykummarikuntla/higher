module.exports = {
    reduce : (elements, cb, startingValue, startingIndex = 0) => {
        if(elements instanceof Array){
            let accumulator = startingValue !== undefined ? startingValue : elements.shift() || [];
            for(let index = startingIndex; index < elements.length; index++) {
                accumulator = cb(accumulator, elements[index]);
            }
            return accumulator;
        }
        else {
            console.log('instanceError: Parameter passed is not an Array');
        }
    }
}
