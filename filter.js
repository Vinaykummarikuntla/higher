
module.exports = {
    filter : (elements, cb, startingIndex = 0) => {
        if(elements instanceof Array){
            const filtrates = [];
            for(let index = startingIndex; index < elements.length; index++) {
                if(cb(elements[index])){
                    filtrates.push(elements[index]);
                }
            }
            return filtrates;
        }
        else {
            console.log('instanceError: Parameter passed is not an Array');
        }
    }
}